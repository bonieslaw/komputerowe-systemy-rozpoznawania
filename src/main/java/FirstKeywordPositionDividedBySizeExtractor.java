public class FirstKeywordPositionDividedBySizeExtractor extends TokenExtractor {
    public Double extractToken(Article article, String[] keywords) {
        String keyword;

        keyword = getFirstKeyword(article, keywords);

        for(double i = 0.0; i < article.getWords().size(); i++) {
            if(article.getWords().get((int)i).equals(keyword)) {
                return 1.0 - (i / article.getWords().size());
            }
        }
        return 2.0;
    }

    private String getFirstKeyword(Article article, String[] keywords) {
        for(int i = 0; i < article.getWords().size(); i++) {
            for(int j = 0; j < keywords.length; j++) {
                if(article.getWords().get(i).equals(keywords[j])) {
                    return keywords[j];
                }
            }
        }
        return "#noKeyword";
    }
}
