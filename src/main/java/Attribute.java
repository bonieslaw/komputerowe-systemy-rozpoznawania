public class Attribute {
    private double doubleAttribute;
    private String stringAttribute;

    Attribute(String str) {
        stringAttribute = str;
    }

    Attribute(double d) {
        doubleAttribute = d;
    }
}
