import java.util.ArrayList;

import static java.lang.Math.abs;
import static java.lang.Math.max;

public class ChebyshevDistance extends Metric {
    @Override
    public double count(ArrayList<Object> setA, ArrayList<Object> setB, SimilarityMeasure similarity) {
        double biggestDistance = 0;
        double newDistance = 0;

        for(int i = 0; i < setA.size(); i++) {
            if(setA.get(i) instanceof Double) {
                newDistance = abs((Double)setA.get(i) - (Double)setB.get(i));
            }
            else if(setA.get(i) instanceof String) {
                newDistance = abs(1.0 - similarity.count(((String)setA.get(i)),((String) setB.get(i))));
            }

            biggestDistance = max(biggestDistance, newDistance);

        }
        return biggestDistance;
    }
}
