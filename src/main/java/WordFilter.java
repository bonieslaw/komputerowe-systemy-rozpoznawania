import java.util.*;

public class WordFilter {
    private String[] nltkStopWords;
    private HashSet<String> stopList;

    public WordFilter() {
        this.nltkStopWords = new String[]
            {"i", "me", "my", "myself", "we", "our", "ours", "ourselves","you", "your", "yours", "yourself",
            "yourselves", "he", "him", "his", "himself", "she", "her", "hers", "herself", "it", "its", "itself", "they",
            "them", "their", "theirs", "themselves", "what", "which", "who", "whom", "this", "that", "these", "those",
            "am", "is", "are", "was", "were", "be", "been", "being", "have", "has", "had", "having", "do", "does",
            "did", "doing", "a", "an", "the", "and", "but", "if", "or", "because", "as", "until", "while", "of", "at",
            "by", "for", "with", "about", "against", "between", "into", "through", "during", "before", "after", "above",
            "below", "to", "from", "up", "down", "in", "out", "on", "off", "over", "under", "again", "further", "then",
            "once", "here", "there", "when", "where", "why", "how", "all", "any", "both", "each", "few", "more", "most",
            "other", "some", "such", "no", "nor", "not", "only", "own", "same", "so", "than", "too", "very", "s", "t",
            "can", "will", "just", "don", "should", "now", ""};

        this.stopList = new HashSet<>();
        stopList.addAll(Arrays.asList(nltkStopWords));
    }

    // usuwa słowa z artykułów wykorzystując słowa stop listy nltk oraz zawierające cyfry
    public List<Article> filterArticles(List<Article> articles) {
        String word;

        for(int i  = 0; i < articles.size(); i++) {
            for(int j = articles.get(i).getWords().size() -1; j >= 0; j-- ) {
                word = articles.get(i).getWords().get(j).toLowerCase();

                // zamiana słów na lower case
                articles.get(i).getWords().set(j, word);

                if(stopList.contains(word)) {
                    articles.get(i).getWords().remove(j);
                    continue;
                }

                if(word.matches("[0-9]+[a-z]*")) {
                    articles.get(i).getWords().remove(j);
                    continue;
                }
            }
        }
        return articles;
    }
}