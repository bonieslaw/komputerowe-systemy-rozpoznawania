import org.apache.commons.lang3.tuple.MutablePair;
// TODO: zacząć używać
public class MostCommonKeywordExtractor extends TokenExtractor {

    public String extractToken(Article article, String[] keyWords) {
        MutablePair<String, Integer> pairOfWordAndOccurrences;
        MutablePair<String, Integer> pairOfMostCommonWordAndOccurrences = new MutablePair<>(null, 0);

        // po słowach kluczowych
        for (int i = 0; i < keyWords.length; i++) {
            pairOfWordAndOccurrences = new MutablePair<>(keyWords[i], 0);
            // po słowach artykułu
            for (int j = 0; j < article.getWords().size(); j++) {
                if (keyWords[i].equals(article.getWords().get(j))) {
                    pairOfWordAndOccurrences.setRight(pairOfWordAndOccurrences.getRight() + 1);
                }
            }

            if (pairOfWordAndOccurrences.getRight() > pairOfMostCommonWordAndOccurrences.getRight()) {
                pairOfMostCommonWordAndOccurrences = new MutablePair<>(pairOfWordAndOccurrences.getLeft(), pairOfWordAndOccurrences.getRight());
            }
        }
        if(pairOfMostCommonWordAndOccurrences.getLeft() == null) {
            return "#noKeyword";
        }
        else {
            return pairOfMostCommonWordAndOccurrences.getLeft();
        }
    }
}
