abstract class SimilarityMeasure {
    public abstract double count(String a, String b);
}
