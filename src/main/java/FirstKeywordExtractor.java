public class FirstKeywordExtractor extends TokenExtractor {
    public String extractToken(Article article, String[] keywords) {
        for(int i = 0; i < article.getWords().size(); i++) {
            for(int j = 0; j < keywords.length; j++) {
                if(article.getWords().get(i).equals(keywords[j])) {
                    return keywords[j];
                }
            }
        }
        return "#noKeyword";
    }
}