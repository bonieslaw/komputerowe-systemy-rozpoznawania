import java.io.IOException;
import java.util.*;
import java.util.List;


public class ArticleAnalizer {
    private List<Article> articles;
    private ArticleParser articleParser;
    private WordFilter filter;
    private String[] keyWords;
    public ArrayList<Article> articlesForTraining = new ArrayList<>();
    public ArrayList<Article> articlesForTesting = new ArrayList<>();
    private KnnAlgorithm knn;
    private Object[][] trainingData;
    private String[] trainingDataClass;
    private Object[][] testData;
    private String[] testDataClassifiers;
    private Metric metric;
    private ArrayList<TokenExtractor> extractors = new ArrayList<>();
    private KeywordGenerator keywordGenerator;
    private String articlesDirectory;
    private double trainingDataRatio; // stosunek danych treningowych do testowych
    private int numberOfKeywords;
    private int k;
    private SimilarityMeasure similarity;
    private String tagType;
    private String numberOfTags;

    public ArticleAnalizer(KeywordGenerator kg, ArrayList<TokenExtractor> ex, String dir, double ratio,
                           int numberOfKeywords, Metric metric, int k, SimilarityMeasure similarity, String tagType,
                           String numberOfTags) {
        keywordGenerator = kg;
        extractors.addAll(ex);
        articlesDirectory = dir;
        trainingDataRatio = ratio;
        this.numberOfKeywords = numberOfKeywords;
        this.metric = metric;
        this.k = k;
        this.similarity = similarity;
        this.tagType = tagType;
        this.numberOfTags = numberOfTags;
    }

    public List<Article> analize() throws IOException {
        articleParser = new ArticleParser(tagType, numberOfTags);
        filter = new WordFilter();

        articles = articleParser.parseSgmFiles(articlesDirectory);
        stemmArticles();
        articles = filter.filterArticles(articles);

        setTrainingAndTestingArticles(trainingDataRatio);

        keyWords = keywordGenerator.generate(articlesForTraining, numberOfKeywords);
        extractAttributesFromArticles(articlesForTraining);
        extractAttributesFromArticles(articlesForTesting);

        normalizeAttributes(articlesForTraining);
        normalizeAttributes(articlesForTesting);

        prepareDataForKnn();

        knn = new KnnAlgorithm(trainingData, trainingDataClass, testData, k, metric, similarity);
        testDataClassifiers = knn.calculate();
        System.out.println("poprawność klasyfikacji: " + calculateCorrectnessInPercentages() + "%");
        
        return articles;
    }

    private void prepareDataForKnn() {
        // dane treningowe [liczba artykułów treningowych][ liczba atrybutów]
        trainingData = new Object[articlesForTraining.size()][articlesForTraining.get(0).getAttributes().size()];
        //etykieta danego artykułu treningowego
        trainingDataClass = new String[articlesForTraining.size()];
        // dane testowe
        testData = new Object[articlesForTesting.size()][articlesForTesting.get(0).getAttributes().size()];

        for(int i = 0; i < trainingData.length; i++) {
            for(int j = 0; j < trainingData[i].length; j++) {
                trainingData[i][j] = articlesForTraining.get(i).getAttributes().get(j);
            }
            trainingDataClass[i] = articlesForTraining.get(i).getTags()[0];
        }

        for(int i = 0; i < testData.length; i++) {
            for (int j = 0; j < testData[i].length; j++) {
                testData[i][j] = articlesForTesting.get(i).getAttributes().get(j);
            }
        }
    }

    private void extractAttributesFromArticles(ArrayList<Article> articles) {
        for(int i = 0; i < articles.size(); i++) {
            for(int j = 0; j < extractors.size(); j++) {
                articles.get(i).addAttribute(extractors.get(j).extractToken(articles.get(i), keyWords));
            }
        }
    }

    private void setTrainingAndTestingArticles(double trainingRatio) {
        int trainingArticlesNumber = (int) (articles.size() * trainingRatio);

        for(int i = 0; i < trainingArticlesNumber; i ++) {
            articlesForTraining.add(articles.get(i));
        }
        for(int i = trainingArticlesNumber; i < articles.size();  i++) {
            articlesForTesting.add(articles.get(i));
        }
    }

    private double calculateCorrectnessInPercentages() {
        double numberOfCorrect = 0;
        double correctnessInPercentages;

        for(int i  = 0; i < testDataClassifiers.length; i++) {
            //TODO: trzeba przerobić na potrzeby większej ilości tagów
            if(testDataClassifiers[i].equals(articlesForTesting.get(i).getTags()[0])) {
                numberOfCorrect++;
            }
        }
        correctnessInPercentages = (numberOfCorrect / articlesForTesting.size()) * 100;

        return correctnessInPercentages;
    }

    private void stemmArticles() {
        for(int i = 0; i < articles.size(); i++) {
            for(int j = 0 ; j < articles.get(i).getWords().size(); j++) {
                String word = articles.get(i).getWords().get(j);
                PorterStemmer stemmer = new PorterStemmer();

                stemmer.add(word.toCharArray(), word.length());
                stemmer.stem();
                word = stemmer.toString();

                if(word.length() > 1) {
                    articles.get(i).setWord(j, word);
                }
            }
        }
    }

    private void normalizeAttributes(ArrayList<Article> articles) {
        // po atrybutach...
        for(int i = 0; i < articles.get(0).getAttributes().size(); i++) {
            //sprawdza, czy dane artykuły są typu Double...
            if(articles.get(0).getAttributes().get(i) instanceof Double) {
                //...jeśli tak, to znormalizuj wszystkie te atrybuty:

                double max = 0;

                // na początek, zbierz wszystkie wartości i wyznacz najwyższą
                for(int j = 0; j < articles.size(); j++) {
                    Double attribute = (Double)articles.get(j).getAttributes().get(i);
                    if(attribute > max) {
                        max = attribute;
                    }
                }

                // następnie iteruj po wszystkich atrybutach raz jeszcze i podziel przez najwyższą
                for(int j = 0; j < articles.size(); j++) {
                    articles.get(j).getAttributes().set(i, ((Double)articles.get(j).getAttributes().get(i) / max));
                }

            }
        }
    }
}
