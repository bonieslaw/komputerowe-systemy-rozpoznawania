import javafx.util.Pair;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.StrictMath.abs;

public class MyKeywordGenerator extends KeywordGenerator {
    // Służy do wygenerowania słów kluczowych.
    // niestety, w przypadku dominującej liczby artykułów o jakiejś etykiecie, wyniki nie będą optymalne, zwłaszcza
    // w przypadku popularnych słów

    @Override
    public String[] generate(ArrayList<Article> articles, int numberOfKeywords) {
        Set<String> wordsSet = new TreeSet<>();
        String[] orderedWords;
        List<ArrayList<Article>> articlesByTokens = new ArrayList<>();
        List<Double> articlesWordsNumberByToken = new ArrayList<>();
        List<HashMap<String, Integer>> listOfArticlesWordNumberMapsForEachTag = new ArrayList<>();
        PriorityQueue<Pair<String, Double>> wordsByImportance = new PriorityQueue<>(new WordsDoublesComparator());
        double wordImportance;
        String word;
        List<String> tokens = new ArrayList<>();

        // utworzenie listy ze wszystkimi rodzajami etykiet
        for(int i = 0; i < articles.size(); i++) {
            tokens.add(articles.get(i).getTags()[0]);
        }
        tokens = tokens.stream().distinct().collect(Collectors.toList());

        // jak już wiemy, ile jest różnych etykiet, to uzupełniamy listy podzielone etykietami pustymi elementami
        for(int i = 0; i < tokens.size(); i++) {
            articlesByTokens.add(new ArrayList<>());
            articlesWordsNumberByToken.add(0.0);
            listOfArticlesWordNumberMapsForEachTag.add(new HashMap<>());
        }

        // dzieli artykuły etykietami

        // po artykułach
        for(int i = 0; i < articles.size(); i++) {
            // po etykietach
            for(int j = 0; j < tokens.size(); j++) {
                if(articles.get(i).getTags()[0].equals(tokens.get(j))) {
                    articlesByTokens.get(j).add(articles.get(i));
                    break;
                }
            }

            // zapisuje set niepowtarzających się słów
            for(int j = 0; j < articles.get(i).getWords().size(); j++) {
                wordsSet.add(articles.get(i).getWords().get(j));
            }
        }

        // zapis ilości słów artykułów dla danych etykiet
        for(int i = 0; i < tokens.size(); i++) {
            for(int j = 0; j < articlesByTokens.get(i).size(); j++) {
                double wordsNumber = articlesByTokens.get(i).get(j).getWords().size();

                articlesWordsNumberByToken.set(i, articlesWordsNumberByToken.get(i) + wordsNumber);
            }
        }

        // zapis mapy: ile słów występuje w danej etykiecie

        // po etykietach
        for(int i = 0; i < tokens.size(); i++) {
            // po artykułach danej etykiety
            for(int j = 0; j < articlesByTokens.get(i).size(); j++) {
                // po słowach danego artykułu
                for(int k = 0; k < articlesByTokens.get(i).get(j).getWords().size(); k++) {
                    word = articlesByTokens.get(i).get(j).getWords().get(k);

                    if(listOfArticlesWordNumberMapsForEachTag.get(i).containsKey(word)) {
                        listOfArticlesWordNumberMapsForEachTag.get(i).put(word, listOfArticlesWordNumberMapsForEachTag.get(i).get(word) + 1);
                    }
                    else {
                        listOfArticlesWordNumberMapsForEachTag.get(i).put(word, 1);
                    }
                }
            }
        }

        // przepisanie setu do tablicy aby można było iterować
        orderedWords = wordsSet.toArray(new String[wordsSet.size()]);

        // po każdym słowie
        for(int i = 0; i < orderedWords.length; i++) {
            wordImportance = 0;
            // po każdej mapie słów-ilości wystąpień
            for(int j = 0; j < listOfArticlesWordNumberMapsForEachTag.size(); j++) {
                // do porównania map, po każdej następnej mapie. Każda wartość słowa zostanie porównana z wartością
                // tego słowa w kolejnych mapach.
                for(int k = j + 1; k < listOfArticlesWordNumberMapsForEachTag.size(); k++) {
                    double a, b;

                    if(listOfArticlesWordNumberMapsForEachTag.get(j).get(orderedWords[i]) == null) {
                        a = 0;
                    }
                    else {
                        a = listOfArticlesWordNumberMapsForEachTag.get(j).get(orderedWords[i]);
                        a = a / articlesWordsNumberByToken.get(j);
                    }

                    if(listOfArticlesWordNumberMapsForEachTag.get(k).get(orderedWords[i]) == null) {
                        b = 0;
                    }
                    else {
                        b = listOfArticlesWordNumberMapsForEachTag.get(k).get(orderedWords[i]);
                        b = b / articlesWordsNumberByToken.get(k);
                    }
                        wordImportance += abs(a - b);
                }
            }

            wordImportance = wordImportance / listOfArticlesWordNumberMapsForEachTag.size();
            wordsByImportance.add(new Pair<>(orderedWords[i], wordImportance));
        }

        String[] keyWords = new String[numberOfKeywords];
        for(int i = 0; i < keyWords.length; i++) {
            keyWords[i] = wordsByImportance.poll().getKey();
        }

        return keyWords;
    }
}
