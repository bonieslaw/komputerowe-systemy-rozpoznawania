abstract public class TokenExtractor {
    abstract Object extractToken(Article article, String[] keywords);
}
