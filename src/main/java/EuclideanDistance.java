import java.util.ArrayList;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class EuclideanDistance extends Metric {

    public double count(ArrayList<Object> setA, ArrayList<Object> setB, SimilarityMeasure sm) {
        double distance = 0;

        for(int i = 0; i < setA.size(); i++) {
            if(setA.get(i) instanceof Double) {
                distance += pow(((Double) setA.get(i)).doubleValue() - ((Double) setB.get(i)).doubleValue() , 2);
            }

            // do sprawdzenia, potwierdzenia itd
            if(setA.get(i) instanceof String) {
                distance += pow(1.0 - sm.count(((String)setA.get(i)),((String) setB.get(i))), 2);
            }
        }
        return sqrt(distance);
    }
}