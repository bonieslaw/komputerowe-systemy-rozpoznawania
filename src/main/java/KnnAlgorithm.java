import javafx.util.Pair;
import org.apache.commons.lang3.tuple.MutablePair;

import java.util.*;
import java.util.stream.Collectors;

public class KnnAlgorithm {
    // lista artykułów z podziałem: klasa - lista list z danymi
    private ArrayList<MutablePair<String, ArrayList<ArrayList<Object>>>> classDataPairsOfTrainingData = new ArrayList<>(); // nie testowane
    private ArrayList<ArrayList<Object>> testData = new ArrayList<>();
    private int k;
    private Metric metric;
    private SimilarityMeasure similarity;

    public KnnAlgorithm(Object[][] trainingSamples, String[] trainingSamplesClass, Object[][] testData, int k,
                        Metric metric, SimilarityMeasure similarity) {

        setClassDataPairsOfTrainingData(trainingSamples, trainingSamplesClass);
        setTestData(testData);
        this.k = k;
        this.metric = metric;
        this.similarity = similarity;
    }

    public String[] calculate() {
        int indexOfLeastNumerousClass = 0;
        int leastNumerousClassSize;
        ArrayList<MutablePair<String, ArrayList<Object>>> startingData = new ArrayList<>();
        String[] testDataClass = new String[testData.size()];
        PriorityQueue<Pair<String,Double>> distancesQueue;
        double distance;

        // obliczenie najmniej licznej grupy danych
        for(int i = 0; i < classDataPairsOfTrainingData.size(); i++) {
            if( classDataPairsOfTrainingData.get(i).getRight().size() <
                classDataPairsOfTrainingData.get(indexOfLeastNumerousClass).getRight().size() ) {
                indexOfLeastNumerousClass = i;
            }
        }

        leastNumerousClassSize = classDataPairsOfTrainingData.get(indexOfLeastNumerousClass).getRight().size();

        // utworzenie grupy danych startowych. Z każdej etykiety będzie ich tyle, ile danych znajdujących się w najmniej
        // licznej grupie
        for(int i = 0; i < classDataPairsOfTrainingData.size(); i++) {
            for(int j = 0; j < leastNumerousClassSize; j++) {
                startingData.add(new MutablePair<>( classDataPairsOfTrainingData.get(i).getLeft(),
                                                    classDataPairsOfTrainingData.get(i).getRight().get(j)) );
            }
        }
        // liczenie odległości danych testowych i klasyfikacja

        // po danych testowych
        for(int i = 0; i < testData.size(); i++) {
            ArrayList<String> lowestDistances = new ArrayList<>();
            distancesQueue = new PriorityQueue<>(new DistanceComparator());


            // po danych startowych
            for(int j = 0; j < startingData.size(); j++) {
                distance = metric.count(testData.get(i), startingData.get(j).getRight(), similarity);
                Pair<String, Double> classDistancePair = new Pair<>(startingData.get(j).getLeft(), distance);
                distancesQueue.add(classDistancePair);
            }

            for(int j = 0; j < k; j++) {
                lowestDistances.add(distancesQueue.poll().getKey());
            }
            testDataClass[i] = mostCommonElement(lowestDistances);
        }

        return testDataClass;
    }

    private void setClassDataPairsOfTrainingData(Object[][] trainingSamples, String[] trainingSamplesClass) {
        ArrayList<Object> trainingData;
        ArrayList<ArrayList<Object>> trainingSamplesData = new ArrayList<>();
        String[] classes = Arrays.stream(trainingSamplesClass).distinct().toArray(String[]::new);

        // po klasach
        for(int i = 0; i < classes.length; i++) {
            trainingSamplesData = new ArrayList<>();
            // po artykułach
            for (int j = 0; j < trainingSamplesClass.length; j++) {
                if(trainingSamplesClass[j].equals(classes[i])) {
                    trainingData = new ArrayList<>();

                    for(int k = 0; k < trainingSamples[j].length; k++) {
                        trainingData.add(trainingSamples[j][k]);
                    }
                    trainingSamplesData.add(trainingData);
                }
            }
            classDataPairsOfTrainingData.add(new MutablePair<>(classes[i], trainingSamplesData));
        }
    }

    private void setTestData(Object[][] testData) {
        ArrayList data;

        for(int i = 0; i < testData.length; i++) {
            data = new ArrayList();
            for(int j = 0 ; j < testData[i].length; j++) {
                data.add(testData[i][j]);
            }
            this.testData.add(data);
        }
    }

    private String mostCommonElement(List<String> list) {
        Map<String, Integer> map = new HashMap<>();

        for(int i=0; i< list.size(); i++) {

            Integer frequency = map.get(list.get(i));
            if(frequency == null) {
                map.put(list.get(i), 1);
            } else {
                map.put(list.get(i), frequency+1);
            }
        }

        String mostCommonKey = null;
        int maxValue = -1;
        for(Map.Entry<String, Integer> entry: map.entrySet()) {

            if(entry.getValue() > maxValue) {
                mostCommonKey = entry.getKey();
                maxValue = entry.getValue();
            }
        }

        return mostCommonKey;
    }
}