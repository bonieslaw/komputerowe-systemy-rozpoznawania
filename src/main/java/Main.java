import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    static KeywordGenerator kw;
    static ArrayList<TokenExtractor> extractors = new ArrayList<>();
    static String dir;
    static double ratio;
    static int numberOfKeywords;
    static Metric metric;
    static int k;
    static SimilarityMeasure similarity;
    static String tagType;
    static String numberOfTags;

    public static void main(String[] args) throws IOException {

        parseArguments(new ArrayList<String>(Arrays.asList(args)));


        List<Article> articles;
        ArticleAnalizer articleAnalizer = new ArticleAnalizer(kw, extractors, dir, ratio, numberOfKeywords, metric, k,
                similarity, tagType, numberOfTags);

       articles = articleAnalizer.analize();
    }

    static void parseArguments(ArrayList<String> arguments) {
        // -keywordExtractor    metoda ekstrakcji słowa kluczowego my/tf/tfidf
        // -ex                   ekstraktory cech    firstKeyword/commonKeyword/firstKWdense/commonKWdense
        // -dir                 folder z dokumentami podlegającymi testowaniu
        // -ratio               stosunek danych treningowych do testowych
        // -kwNum               liczba słów kluczowych
        // -metric              wybrana metryka
        // -k                   k najbliższych sąsiadów
        // -similarity          miara podobieństwa  GeneralizedNGram/trigram
        // -tag                 etykieta według której odbędzie się klasyfikacja
        // -tagNum              ile sztuk wybranej etykiety może zawierać artykuł: one, moreThanOne, two

        int index;

        index = arguments.indexOf("-keywordExtractor") + 1;

        if(arguments.get(index).equals("tf")) {
            kw = new TermFrequency();
        }
        else if(arguments.get(index).equals("tfidf")) {
            kw = new TFIDF();
        }
        else {
            kw = new MyKeywordGenerator();
        }

        index = arguments.indexOf("-ex");

        for(int i = index + 1; i < arguments.size(); i++) {
            if(!(arguments.get(i).substring(0,1) == "-")) {
                if(arguments.get(i).equals("firstKeyword")) {
                    extractors.add(new FirstKeywordExtractor());
                }
                else if(arguments.get(i).equals("commonKeyword")) {
                    extractors.add(new MostCommonKeywordExtractor());
                }
                else if(arguments.get(i).equals("commonKWdense")) {
                    extractors.add(new CommonKeywordPositonDividedBySizeExtractor());
                }
                else if(arguments.get(i).equals("firstKWdense")) {
                    extractors.add(new FirstKeywordPositionDividedBySizeExtractor());
                }
                else {
                    break;
                }
            }
            else {
                break;
            }
        }

        index = arguments.indexOf("-dir") + 1;
        dir = arguments.get(index);

        index = arguments.indexOf("-ratio") + 1;
        ratio = new Double(arguments.get(index)).doubleValue();

        index = arguments.indexOf("-kwNum") + 1;
        numberOfKeywords = new Integer(arguments.get(index)).intValue();

        index = arguments.indexOf("-metric") + 1;
        if(arguments.get(index).equals("chebyshev")) {
            metric = new ChebyshevDistance();
        }
        else if(arguments.get(index).equals("euclidean")) {
            metric = new EuclideanDistance();
        }
        else if(arguments.get(index).equals("manhattan")){
            metric = new ManhattanDistance();
        }

        index = arguments.indexOf("-k") + 1;
        k = new Integer(arguments.get(index)).intValue();

        index = arguments.indexOf("-similarity") + 1;
        if(arguments.get(index).equals("GeneralizedNGram")) {
            similarity = new GeneralizedNGramsMeasure();
        }
        else if(arguments.get(index).equals("trigram")){ //trigram
            similarity = new TriGramsMeasure();
        }

        index = arguments.indexOf("-tag") + 1;
        tagType = arguments.get(index);

        index = arguments.indexOf("-tagNum") + 1;
        numberOfTags = arguments.get(index);
    }
}
