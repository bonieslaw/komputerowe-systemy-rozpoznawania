import static java.lang.Math.min;
import static java.lang.Math.pow;

public class TriGramsMeasure extends SimilarityMeasure {
    @Override
    public double count(String a, String b) {
        double n;
        int counter = 0;
        double result;

        n = min(a.length(), b.length());
        a = a.toLowerCase();
        b = b.toLowerCase();

        // po pierwszym słowie...
        for(int j = 0; j < a.length() - 3 + 1; j++) {
            // po drugim słowie...
            for(int k = 0; k < b.length() - 3 + 1; k++) {
                if(a.substring(j, j + 3).equals(b.substring(k, k + 3))) {
                    counter++;
                    break;
                }
            }
        }
        result = (1.0 / (n - 2.0)) * counter;
        return result;
    }
}
