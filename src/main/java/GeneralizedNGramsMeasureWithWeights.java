import static java.lang.Math.min;
import static java.lang.Math.pow;
import static java.lang.StrictMath.max;

public class GeneralizedNGramsMeasureWithWeights extends SimilarityMeasure {

    @Override
    public double count(String a, String b) {
        int n;
        int N;
        int counter = 0;
        String shorterWord;
        String longerWord;

        n = min(a.length(), b.length());
        N = max(a.length(), b.length());

        if(a.length() > b.length()) {
            longerWord = a;
            shorterWord = b;
        }
        else {
            longerWord = b;
            shorterWord = a;
        }

        // po n-gramach...
        for(int i = 1; i <= n; i++) {
            // po pierwszym słowie...
            for(int j = 0; j < longerWord.length() - i + 1; j++) {
                // po drugim słowie...
                for(int k = 0; k < shorterWord.length() - i + 1; k++) {
                    if(longerWord.substring(j, j + i).equals(shorterWord.substring(k, k + i))) {
                        counter++;
                        break;
                    }
                }
            }
        }

        return (2 / ((pow(N, 2) + N)) ) * counter;
    }
}
