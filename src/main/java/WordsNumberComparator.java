import javafx.util.Pair;

import java.util.Comparator;

public class WordsNumberComparator implements Comparator<Pair<String,Integer>> {
    @Override
    public int compare(Pair<String, Integer> a, Pair<String, Integer> b) {
        if(a.getValue() < b.getValue()) {
            return 1;
        }
        else if(a.getValue() > b.getValue()) {
            return -1;
        }
        return 0;
    }
}
