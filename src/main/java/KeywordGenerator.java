import java.util.ArrayList;

abstract class KeywordGenerator {
    abstract public String[] generate(ArrayList<Article> articles, int numberOfKeywords);
}
