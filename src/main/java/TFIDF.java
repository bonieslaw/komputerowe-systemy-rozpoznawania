import javafx.util.Pair;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeSet;

public class TFIDF extends KeywordGenerator {
    @Override
    public String[] generate(ArrayList<Article> articles, int numberOfKeywords) {
        Set<String> wordsSet = new TreeSet<>();
        String[] uniqueWords;
        PriorityQueue<Pair<String, Double>> wordsByImportance = new PriorityQueue<>(new WordsDoublesComparator());
        String[] keyWords = new String[numberOfKeywords];

        // zapisuje set niepowtarzających się słów:
        for(int i = 0; i < articles.size(); i++) {
            for(int j = 0; j < articles.get(i).getWords().size(); j++) {
                wordsSet.add(articles.get(i).getWords().get(j));
            }
        }

        // lista wszystkich słów w tablicy:
        uniqueWords = wordsSet.toArray(new String[wordsSet.size()]);

        // zliczanie ważności słowa i dodawanie do kolejki ważnością:
        for(int i = 0; i < uniqueWords.length; i++) {
            double wordImportance = countWordImportance(articles, uniqueWords[i]);

            wordsByImportance.add(new Pair<>(uniqueWords[i], wordImportance));
        }

        // zapis słów kluczowych:
        for(int i = 0; i < keyWords.length; i++) {
            keyWords[i] = wordsByImportance.poll().getKey();
        }

        return keyWords;
    }

    private double countTermFrequencies(ArrayList<Article> articles, String wordToCount) {
        double termFrequencies = 0;

        for(int j = 0; j < articles.size(); j++) {
            termFrequencies += countTermFrequency(articles.get(j), wordToCount);
        }

        return termFrequencies;
    }

    // zlicza TF dla danego słowa w danym artykule
    private double countTermFrequency(Article article, String wordToCount) {
        double wordAmount = 0;
        double termFrequency;

        for(int j = 0; j < article.getWords().size(); j++) {
            String word = article.getWords().get(j);

            if(word == wordToCount) {
                wordAmount++;
            }
        }

        termFrequency = wordAmount / article.getWords().size();

        return termFrequency;
    }

    // zlicza IDF dla danego słowa w danym artykule
    private double countInverseDocumentFrequency(ArrayList<Article> articles, String wordToCount) {
        double inverseDocumentFrequency;
        double numberOfArticles; // D
        double numberOfArticlesConsistingWord = 0;

        numberOfArticles = articles.size();

        // zliczanie liczby artykułów zawierających przynajmniej jedno wystąpienie danego słowa
        for(int i = 0; i < numberOfArticles; i++) {
            for(int j = 0; j < articles.get(i).getWords().size(); j++) {
                String word = articles.get(i).getWords().get(j);

                if(word == wordToCount) {
                    numberOfArticlesConsistingWord++;
                    break;
                }
            }
        }

        inverseDocumentFrequency = Math.log( numberOfArticles / numberOfArticlesConsistingWord);

        return inverseDocumentFrequency;
    }

    // zlicza ważność słowa za pomocą TF + IDF
    private double countWordImportance(ArrayList<Article> articles, String wordToCount) {
        double termFrequencies = countTermFrequencies(articles, wordToCount);
        double inverseDocumentFrequency = countInverseDocumentFrequency(articles, wordToCount);
        double wordImportance = termFrequencies * inverseDocumentFrequency;

        return wordImportance;
    }
}