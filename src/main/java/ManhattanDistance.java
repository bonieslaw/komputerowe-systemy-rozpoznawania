import java.util.ArrayList;
import static java.lang.Math.*;

public class ManhattanDistance extends Metric {
    @Override
    public double count(ArrayList<Object> setA, ArrayList<Object> setB, SimilarityMeasure similarity) {
        double distance = 0;

        for(int i = 0; i < setA.size(); i++) {
            if(setA.get(i) instanceof Double) {
                distance += abs((Double)setA.get(i) - (Double)setB.get(i));
            }

            if(setA.get(i) instanceof String) {
                distance += abs(1.0 - similarity.count(((String)setA.get(i)),((String) setB.get(i))));
            }
        }
        return distance;
    }
}
