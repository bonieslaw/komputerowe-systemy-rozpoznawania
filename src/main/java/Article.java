import java.util.ArrayList;

public class Article {
    private String[] tags;
    private String text;
    private ArrayList<String> words;
    private ArrayList<Object> attributes = new ArrayList<>();   // cechy po ekstrakcji

    Article(Article article) {
        this.text = article.text;
        this.tags = article.tags;
        this.words = article.words;
    }

    Article() {
        this.text = null;
        this.tags = null;
    }

    void setText(String text) {
        this.text = text;
    }

    void setTags(String[] tags) {
        this.tags = tags;
    }

    public String[] getTags() {
        return tags;
    }

    String getText() {
        return text;
    }

    ArrayList<String> getWords() {
        return words;
    }

    void setWords(ArrayList words) {
        this.words = words;
    }

    void setWord(int index, String word) {
        this.words.set(index, word);
    }

    void setTag(int index, String tag) {
        tags[index] = tag;
    }

    public ArrayList<Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(ArrayList<Object> attributes) {
        this.attributes = attributes;
    }

    public void addAttribute(Object attribute) {
        this.attributes.add(attribute);
    }
}
