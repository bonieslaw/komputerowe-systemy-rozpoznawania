import javafx.util.Pair;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.StrictMath.abs;

public class TermFrequency extends KeywordGenerator {
    @Override
    public String[] generate(ArrayList<Article> articles, int numberOfKeywords) {
        List<String> tokens = new ArrayList<>();
        List<ArrayList<Article>> articlesByTokens = new ArrayList<>();
        List<Double> articlesWordsNumberByToken = new ArrayList<>();
        List<HashMap<String, Integer>> listOfArticlesWordNumberMapsForEachTag = new ArrayList<>();
        Set<String> wordsSet = new TreeSet<>();
        String word;
        String[] orderedWords;
        double wordImportance;
        PriorityQueue<Pair<String, Double>> wordsByImportance = new PriorityQueue<>(new WordsDoublesComparator());

        // utworzenie listy ze wszystkimi rodzajami etykiet
        for (int i = 0; i < articles.size(); i++) {
            tokens.add(articles.get(i).getTags()[0]);
        }
        tokens = tokens.stream().distinct().collect(Collectors.toList());

        // jak już wiemy, ile jest różnych etykiet, to uzupełniamy listy podzielone etykietami pustymi elementami
        for (int i = 0; i < tokens.size(); i++) {
            articlesByTokens.add(new ArrayList<>());
            articlesWordsNumberByToken.add(0.0);
            listOfArticlesWordNumberMapsForEachTag.add(new HashMap<>());
        }

        // dzieli artykuły etykietami

        // po artykułach
        for (int i = 0; i < articles.size(); i++) {
            // po etykietach
            for (int j = 0; j < tokens.size(); j++) {
                if (articles.get(i).getTags()[0].equals(tokens.get(j))) {
                    articlesByTokens.get(j).add(articles.get(i));
                    break;
                }
            }

            // zapisuje set niepowtarzających się słów
            for(int j = 0; j < articles.get(i).getWords().size(); j++) {
                wordsSet.add(articles.get(i).getWords().get(j));
            }
        }

        // po etykietach
        for(int i = 0; i < tokens.size(); i++) {
            // po artykułach danej etykiety
            for(int j = 0; j < articlesByTokens.get(i).size(); j++) {
                // po słowach danego artykułu
                for(int k = 0; k < articlesByTokens.get(i).get(j).getWords().size(); k++) {
                    word = articlesByTokens.get(i).get(j).getWords().get(k);

                    if(listOfArticlesWordNumberMapsForEachTag.get(i).containsKey(word)) {
                        listOfArticlesWordNumberMapsForEachTag.get(i).put(word, listOfArticlesWordNumberMapsForEachTag.get(i).get(word) + 1);
                    }
                    else {
                        listOfArticlesWordNumberMapsForEachTag.get(i).put(word, 1);
                    }
                }
            }
        }

        // przepisanie setu do tablicy aby można było iterować
        orderedWords = wordsSet.toArray(new String[wordsSet.size()]);

        // ważność słowa jest obliczana poprzez dodanie ilorazów liczby wystąpień słowa kluczowego do ilości wyrazów
        // w danym artykule

        for(int i = 0; i < orderedWords.length; i++) {
            wordImportance = 0;
            // po każdej mapie słów-ilości wystąpień
            for(int j = 0; j < listOfArticlesWordNumberMapsForEachTag.size(); j++) {
                    double wordAmountInArticle;

                    if(listOfArticlesWordNumberMapsForEachTag.get(j).get(orderedWords[i]) == null) {
                        wordAmountInArticle = 0;
                    }
                    else {
                        wordAmountInArticle = listOfArticlesWordNumberMapsForEachTag.get(j).get(orderedWords[i]);
                        wordAmountInArticle = wordAmountInArticle / articlesWordsNumberByToken.get(j);
                    }

                    wordImportance += wordAmountInArticle;
            }

            wordsByImportance.add(new Pair<>(orderedWords[i], wordImportance));
        }

        String[] keyWords = new String[numberOfKeywords];

        for(int i = 0; i < keyWords.length; i++) {
            keyWords[i] = wordsByImportance.poll().getKey();
        }
        return keyWords;
    }
}
