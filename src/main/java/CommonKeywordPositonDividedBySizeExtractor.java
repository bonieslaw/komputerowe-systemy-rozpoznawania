import org.apache.commons.lang3.tuple.MutablePair;

public class CommonKeywordPositonDividedBySizeExtractor extends TokenExtractor{

    public Double extractToken(Article article, String[] keywords) {
        String keyword;

        keyword = getMostCommonKeyword(article, keywords);

        for(double i = 0.0; i < article.getWords().size(); i++) {
            if(article.getWords().get((int)i).equals(keyword)) {
                return 1.0 - (i / article.getWords().size());
            }
        }
        return 2.0;
    }

    private String getMostCommonKeyword(Article article, String[] keywords) {
        MutablePair<String, Integer> pairOfWordAndOccurrences;
        MutablePair<String, Integer> pairOfMostCommonWordAndOccurrences = new MutablePair<>(null, 0);

        // po słowach kluczowych
        for (int i = 0; i < keywords.length; i++) {
            pairOfWordAndOccurrences = new MutablePair<>(keywords[i], 0);
            // po słowach artykułu
            for (int j = 0; j < article.getWords().size(); j++) {
                if (keywords[i].equals(article.getWords().get(j))) {
                    pairOfWordAndOccurrences.setRight(pairOfWordAndOccurrences.getRight() + 1);
                }
            }

            if (pairOfWordAndOccurrences.getRight() > pairOfMostCommonWordAndOccurrences.getRight()) {
                pairOfMostCommonWordAndOccurrences = new MutablePair<>(pairOfWordAndOccurrences.getLeft(), pairOfWordAndOccurrences.getRight());
            }
        }
        if(pairOfMostCommonWordAndOccurrences.getLeft() == null) {
            return "#noKeyword";
        }
        else {
            return pairOfMostCommonWordAndOccurrences.getLeft();
        }
    }
}
