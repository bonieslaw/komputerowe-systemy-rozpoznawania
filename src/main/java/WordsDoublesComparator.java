import javafx.util.Pair;

import java.util.Comparator;

public class WordsDoublesComparator implements Comparator<Pair<String, Double>> {
    public int compare(Pair<String, Double> a, Pair<String, Double> b) {
        if(a.getValue() < b.getValue()) {
            return 1;
        }
        else if(a.getValue() > b.getValue()) {
            return -1;
        }
        return 0;
    }
}
