import java.util.ArrayList;

abstract class Metric {
    public abstract double count(ArrayList<Object> setA, ArrayList<Object> setB, SimilarityMeasure similarity);
}
