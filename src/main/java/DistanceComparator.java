import javafx.util.Pair;

import java.util.Comparator;

//rosnąco
public class DistanceComparator implements Comparator<Pair<String, Double>> {
    @Override
    public int compare(Pair<String, Double> a, Pair<String, Double> b) {
        if(a.getValue() > b.getValue()) {
            return 1;
        }
        else if(a.getValue() < b.getValue()) {
            return -1;
        }
        return 0;
    }
}
