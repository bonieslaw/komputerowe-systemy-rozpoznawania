import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

 class ArticleParser {
     private String tagType;
     private String numberOfTags;

     ArticleParser(String tagType, String numberOfTags) {
         this.tagType = tagType;
         this.numberOfTags = numberOfTags;
     }

     List<Article> parseSgmFiles(String sgmFilesDirectory) throws IOException {
        File directory = new File(sgmFilesDirectory);
        List<File> sgmFilesList;
        List<Article> articles = new ArrayList<>();

        sgmFilesList = getListOfSgmFiles(directory);

        for (File i : sgmFilesList) {
            articles.addAll( parseFile(i) );
        }
        return articles;
    }

    private List<File> getListOfSgmFiles(File directory) {
        File[] listOfFiles = directory.listFiles();
        List<File> sgmFiles = new ArrayList<>();

        for(File i : listOfFiles) {
            sgmFiles.add(i);
        }

        return sgmFiles;
    }

    private List<Article> parseFile(File file) throws IOException {
        ArrayList<Article> articles = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;

        boolean isTextLine = false;
        boolean isArticleLine = false;
        String articleTagStart = "<REUTERS";
        String articleTagEnd = "</REUTERS>";
        String textTagEnd = "</BODY>";
        String textTagStart = "<BODY>";
        String tagTypeStart = "<" + tagType + ">";
        String dTagStart = "<D>";
        String articleText = "";
        Article article = new Article();
        String[] viablePlacesTags = {"west-germany", "usa", "france", "uk", "canada", "japan"};
        String[] viableTopicsTags = {"acq", "crude", "money-fx", "interest", "trade", "earn"};
        boolean isTagViable = false;

        while ( (line = br.readLine() ) != null) {
            if(isArticleLine) {
                if(isTextLine) {
                    if(line.contains(textTagEnd)) {
                        isTextLine = false;
                        article.setText(articleText);
                        articleText = "";
                    }
                    else {
                        articleText = articleText + line + "\n";
                    }
                }
                else if(line.contains(textTagStart)) {
                    articleText = line.replaceAll(".*<BODY>", "");
                    isTextLine = true;
                    articleText = articleText + line + "\n";
                }
                else if(line.contains(tagTypeStart)) {
                    if(line.contains(dTagStart)) {
                        article.setTags( getPlacesTag(line) );
                        if(numberOfTags.equals("one")) {
                            // więcej niż jeden tag, nie dodawać artykułu do listy
                            if(article.getTags().length > 1) {
                                isArticleLine = false;
                                article = new Article();
                            }
                        }
                        else if(numberOfTags.equals("two")) {
                            // nie dwa tagi - nie dodawać artykułu
                            if(article.getTags().length != 2) {
                                isArticleLine = false;
                                article = new Article();
                            }
                            else {
                                //łączenie tagów:
                                String mergedTags = "";
                                String[] newTags = {""};


                                for (int i = 0; i < article.getTags().length; i++) {
                                    mergedTags += article.getTags()[i];
                                }
                                newTags[0] = mergedTags;
                                article.setTags(newTags);
                            }
                        }
                        else if(numberOfTags.equals("moreThanOne")) {
                            if(article.getTags().length <= 1) {
                                isArticleLine = false;
                                article = new Article();
                            }
                            else {
                                //łączenie tagów:
                                String mergedTags = "";
                                String[] newTags = {""};


                                for (int i = 0; i < article.getTags().length; i++) {
                                    mergedTags += article.getTags()[i];
                                }
                                newTags[0] = mergedTags;
                                article.setTags(newTags);
                            }
                        }
                    }
                    // nie ma etykiet places, nie dodawać tego artykułu do listy
                    else {
                        isArticleLine = false;
                        article = new Article();
                    }
                }
                else if( line.contains(articleTagEnd) ) {
                    // nie ma tekstu, nie dodajemy tego artykułu
                    if(article.getText() == null) {
                        isArticleLine = false;
                        article = new Article();
                    }
                    else {
                        if(tagType.equals("PLACES")) {
                            for(int i = 0; i < article.getTags().length; i++) {
                                //TODO: te pętle można zapisać lepiej
                                for(int j = 0; j < viablePlacesTags.length; j++) {
                                    if(article.getTags()[i].equals(viablePlacesTags[j])) {
                                        isTagViable = true;
                                        break;
                                    }
                                }
                            }

                        }
                        else if(tagType.equals("TOPICS")) {
                            for(int i = 0; i < article.getTags().length; i++) {
                                //TODO: te pętle można zapisać lepiej
                                //for(int j = 0; j < viableTopicsTags.length; j++) {
                                    //if(article.getTags()[i].equals(viableTopicsTags[j])) {
                                        isTagViable = true;
                                       // break;
                                    //}
                               // }
                            }

                        }
                        else {
                            isTagViable = true;
                        }
                        article.setWords((ArrayList) wordsToLowerCase(separateTextToWords(article.getText())));

                        if(isTagViable) {
                            articles.add(new Article(article));
                        }
                        article = new Article();
                        isTagViable = false;
                    }
                }
            }
            else {
                if(line.contains(articleTagStart)) {
                    isArticleLine = true;
                }
            }
        }

        return articles;
    }

    // Usuwa z tekstu specjalne znaki atrybutów i zwraca tablice nazw miejsc
    private String[] getPlacesTag(String text) {
        String[] places;

        String tagTypeBegin =  "<" + tagType + ">";
        String tagTypeEnd =  "</" + tagType + ">";
        String dTagBegin = "<D>";
        String dTagEnd = "</D>";

        // usuwam zbędne tagi z tekstu
        text = text.replaceAll(tagTypeBegin, "");
        text = text.replaceAll(tagTypeEnd, "");
        text = text.replaceAll(dTagBegin, "");

        places = text.split(dTagEnd);

        return places;
    }

    private List<String> separateTextToWords(String text) {
        List<String> list = new ArrayList<>(Arrays.asList(text.split("\\W+")));
        return list;
    }

    private List<String> wordsToLowerCase(List<String> words) {
        for (int i = 0; i < words.size(); i++) {
            words.set(i, words.get(i).toLowerCase());
        }
        return words;
    }
}
